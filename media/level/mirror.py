import sys

def args():
	global infile, outfile
	if(len(sys.argv) == 3):
		return sys.argv[1],sys.argv[2]
	else:
		return '',''

def main():
	infile, outfile = args()
	if infile != '' and outfile != '':
		inf = open(infile,'r')
		outf = open(outfile,'w')
		for line in inf:
			
			t = line.split(',')
			if(len(t) == 7):
				t[3] = ' '+str(-float(t[3]))+' '
			outf.write(','.join(t))
		inf.close()
		outf.close()
if __name__ == "__main__":
	main()
