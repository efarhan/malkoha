#!/usr/bin/python
'''Script to convert the ogremesh exported from blender'''
try:
	import xml.etree.ElementTree as etree
except ImportError:
	print "Install lxml"
	exit(1)
import sys
import copy	
framerate = 25.0			
def arg_management():
	'''manage the given arguments'''
	global framerate
	help = '''Options:
By default :
ninja
anim_file input_file output_file [framerate]
-i input file
-o output file
-a anim file
-f framerate'''
	input_file = 'models/ant01.ms3d.m.skeleton.xml'
	output_file = 'models/ant01.skeleton.xml'
	anim_file = 'keyframes/ant.txt'
	length = len(sys.argv)
	for arg in sys.argv:
			if(arg == '--help'):
				print help
				exit(1)
	if(length == 4 or length == 5):
		anim_file = sys.argv[1]
		input_file = sys.argv[2]
		output_file = sys.argv[3]
	if(length == 5):
		framerate = float(sys.argv[4])	
	if(length > 4):
		filename = {}
		for option in ['-i', '-o', '-a', '-f']:
			try:
				option_index = sys.argv.index(option)
				filename[option] = sys.argv[option_index+1]
			except ValueError:
				pass 
		try:
			input_file = filename['i']
		except KeyError:
			pass
		try:
			output_file = filename['o']
		except KeyError:
			pass
		try:
			anim_file = filename['-a']
		except KeyError:
			pass 
		try:
			framerate = float(filename['-f'])
		except KeyError:
			pass
		
	return (anim_file, input_file, output_file)
		
def open_xml(filename):
	tree = etree.parse(filename)
	return tree
	
def parse_line(line):
	elem = line.split(',')
	if(len(elem)>= 3):
		return (elem[0], (int(elem[1]), int(elem[2])))
	return (None, None)
def parse_animation(filename):
	f = open(filename, 'rb')
	anim = {}
	for line in f:
		index = line.find('--')
		(name, value) = parse_line(line[:index])
		if(name != None):
			anim[name] = value
	return anim
	
def write_xml(tree, filename):
	root = tree.getroot()
	tree.write(filename)
def add_animations(tree, anim):
	root = tree.getroot()
	animations = root.find('animations')
	if animations == None :
		return
	base_anim = animations.find('animation')
	elements = []
	for new_anim_name in anim.keys():
		print new_anim_name,
		new_anim = etree.Element('animation', 
		attrib={'name' : new_anim_name})
		elements.append(new_anim)
		new_tracks = etree.SubElement(new_anim, 'tracks')
		(start, end) = anim[new_anim_name]
		print (start,end),
		tracks = base_anim.find('tracks')
		l = None
		for track in tracks:
			new_joint = etree.SubElement(new_tracks, 'track')
			new_joint.attrib['bone'] = track.attrib['bone']
			new_keyframes = etree.SubElement(new_joint, 'keyframes')
			keyframes = track.find('keyframes')
			i = 0
			for keyframe in keyframes.getchildren()[start:end]:
				new_keyframe = copy.deepcopy(keyframe)
				new_keyframe.attrib['time'] = str(float(i)/framerate)
				new_keyframes.append(new_keyframe)
				i+=1
			if(l==None):
				l = str(float(i)/framerate)
		new_anim.attrib['length'] = l
		print ''
	for elem in elements:
		animations.append(elem)
				
		
	
def main():
	(anim_filename, xml_filename, out_filename) = arg_management()
	anim = parse_animation(anim_filename)
	tree = open_xml(xml_filename)
	add_animations(tree, anim)
	write_xml(tree, out_filename)
if __name__ == '__main__':
	main()
	
	
