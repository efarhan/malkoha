#ifndef ANT_HEADER
#define ANT_HEADER
#include <Ogre.h>
#include <OIS.h>
#include <AdvancedOgreFramework.hpp>
#include <serial.h>

	typedef enum 
	{
		ANT_WALK,
		ANT_ATTACK,
		ANT_STILL,
		ANT_ANIM_LENGTH
	} ANT_ANIM;

#define QTE_TIME 2000
#define TIME_TO_FALL 500
class Ant
{
public:
	Ant(Ogre::String name, Ogre::SceneManager* sceneMgr);
	~Ant();
	void update(Ogre::Real deltatime);

	void setNode(Ogre::SceneNode* node);
	void setEntity(Ogre::Entity* ent);
	void die();
	bool addQTETime(Ogre::Real dTime);
	void enterQTEMode();
	void setupAnimation();
	Ogre::Vector3 getPosition();

	bool dead;
	float y;
private:
	Ogre::String mName;
	Ogre::SceneNode* mNode;
	Ogre::Entity* mEntity;
	Ogre::SceneManager *mSceneMgr;

	Ogre::AnimationState* mAnims[ANT_ANIM_LENGTH];
	int mAnimIndex;
	Ogre::Real QTETime;
	float timeToFall;

};
#endif