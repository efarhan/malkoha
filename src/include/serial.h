
#ifndef SERIAL_H__
#define SERIAL_H__

#include <stdint.h>
#include <stdio.h>
#include <SFML/System.hpp>
#include <time.h>
#include <stdint.h>
#include <errno.h>
#include "AdvancedOgreFramework.hpp"




//conf


//buffer when reading serial
#define BUFFER_SIZE 256

//program will wait this amount of ms if it can't access serial (instead of overloading CPU)
//also used by the ASync loop
#define LOOP_WAIT_TIME 10 

//#define TEST_FILE //writes dummy data into file, to test file writing w/o arduino

//value used to convert float to integer when saving in binary
#define FLOAT_AS_INTEGER_RATIO 10000

//amount of ms in a clock tick (usually 1)
#define PERIOD (1000/CLOCKS_PER_SEC)

//maximum amount of "0" for the beat value inside of a beat
//(during a continious beat, there will be sometimes "false" 0 value for the beat:
//1,1,1,0,1
//Where it should all be 1. This is the maximum amount of 0 that will be "skipped" without
//considering we are not in the same beat.
#define MAX_BEAT_SKIP 5


//conf ends here


//converts float to integer without losing precision (beware overflow with big values though)
#define toInteger(x) ((((int)(x))*FLOAT_AS_INTEGER_RATIO) + (((int)((x)*FLOAT_AS_INTEGER_RATIO)) % FLOAT_AS_INTEGER_RATIO))
#define toFloat(x) ((((float)((x)%FLOAT_AS_INTEGER_RATIO))/FLOAT_AS_INTEGER_RATIO) + (float)((x)/FLOAT_AS_INTEGER_RATIO))

#define COM_NOT_FOUND -1
#define OPEN_FAILED -2
#define CONFIGURE_FAILED -3
#define UNSUPPORTED -4
#define BUFFER_OVERFLOW -5
#define ALREADY_OPENED -6

typedef struct {
	float ecg;
	bool beat;
	float conductance;
} ArduinoData;

typedef struct {
	uint32_t ecg;
	uint8_t beat;
	uint32_t conductance;

	uint32_t time;

	uint8_t threat;
} ConstantSize_FileData;

typedef struct {
	ArduinoData data;
	int time;
	int threat;
} FileData;

typedef struct ASyncData_ {
	ArduinoData data;
	int time;
	struct ASyncData_ *next;
} ASyncData;

//opens a serial to read data (set this to the COM port to which the Arduino device is connected)
//until a serial has been opened, the program will just read NULL data (you can still read them though =p)
int openSerial(const char *comName);

//close a serial properly (if you wanna open it with another program)
//this function is called from within openSerial, no needs to call it if you mean to reopen a serial with that function
int closeSerial();

//read the most recent data instance sent by the Arduino device
void readData(ArduinoData *normalData, ArduinoData *asyncData);

//terminates the main loop and properly close any files opened with openFile
int close();

//main loop to read from the Arduino device. You will need to call openSerial and openFile externally for it to function properly
//(you can call it afterwards though, the loop won't crash if serial isn't opened)
int serialMainLoop();

//prints the most recent data instance sent by the Arduino device using stdout
void printData();

//can be used to convert files between text and binary format
//if binaryToTxt is true, then we're converting a binary file to a text one, if false, we're converting a text file to a binary one (obviously)
int convertFile(const char *input, const char *output, bool binaryToTxt);

//opens file to write arduino data.
//append to true if we should append the data at the end of an existing file (if false will create a new file)
//text to true if data should be written in a human readable text format (otherwise binary data will be written)
int openFile(const char *fileName, bool append, bool text);

int closeFile();

void writeToFile(FILE *f, FileData *d, bool text);

void setThreat(int value);

//opens a pre-recorded file of data to use a asynchronous beat
int openAsyncDataFile(const char *fileName, bool text);

//async loop, will update beat value with the file loaded with openAsyncDataFile(...)
int aSyncMainLoop();

//terminates thread
int closeAsync();

int test();


#ifdef _WIN32
	#include <windows.h>
#else
	#include <unistd.h>
#endif
















#endif //SERIAL_H__