
#ifndef LEVEL_H__
#define LEVEL_H__
#include <stdio.h>
#include <errno.h>
#include <Ogre.h>
#include <AdvancedOgreFramework.hpp>
#include <string.h>
#include <Ant.hpp>
#include <Tutorial.hpp>

class LevelReader {

public:
	LevelReader(Ogre::String name, Ogre::SceneManager* sceneMgr, std::vector<int>* checkPoints);
	~LevelReader();
	
	int readFile();
	void setAntList(std::vector<Ant*>* antlist);
	void setEnd(int* end);
	void setTutorial(std::vector<Tutorial*>* tutorial);
private:
	Ogre::String mName;
	Ogre::SceneManager* mSceneMgr;
	std::vector<int>* mCheckpoints;
	
	int readLine(FILE *f);

	std::vector<Ant*>* mAntList;
	int* mEnd;

	std::vector<Tutorial*>* mTutorial;
};













#endif //LEVEL_H__