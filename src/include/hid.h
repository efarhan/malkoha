#ifndef HID_HPP
#define HID_HPP
#include <stdio.h>
#include <wchar.h>
#include <string.h>
#include <stdlib.h>
#include "hidapi.h"
#include <SFML/System.hpp>
#include <AdvancedOgreFramework.hpp>

// Headers needed for sleeping.
#ifdef _WIN32
	#include <windows.h>
#else
	#include <unistd.h>
#endif

/*
x and y are between -1 and 1 and represent the "position" of the stick
angle is between 0 and 1 and represents the angle of the stick (0 = vertical, 1 = horizontal)

*/
typedef struct {
	float x,y,angle;
	bool button; //true if button is pressed, false otherwise
} Data;


/*
returns pointer to data, or NULL if anything failed.
calling this function will free the previously returned pointer.
*/
Data* getData(); 
void setData(int* newData);

int openHID();
int loopHID();
void closeApp();
#endif