#ifndef MALKOHA_CAMERA_HPP
#define MALKOHA_CAMERA_HPP
#include <Ogre.h>
#define TIGHTNESS 0.1f
class MalkohaCamera
{
public:
	MalkohaCamera(Ogre::String name, Ogre::SceneManager* sceneMgr, Ogre::Camera* camera);
	~MalkohaCamera();

	Ogre::Vector3 getCameraPosition ();
	void setTightness (Ogre::Real tightness);
	Ogre::Real getTightness ();
	void instantUpdate (Ogre::Vector3 cameraPosition, Ogre::Vector3 targetPosition);
	void update (Ogre::Real elapsedTime, Ogre::Vector3 cameraPosition, Ogre::Vector3 targetPosition);
protected:
	Ogre::SceneNode *mTargetNode; // The camera target
    Ogre::SceneNode *mCameraNode; // The camera itself
    Ogre::Camera *mCamera; // Ogre camera
 
    Ogre::SceneManager *mSceneMgr;
    Ogre::String mName;
	bool mOwnCamera;
	Ogre::Real mTightness;
};
#endif