//|||||||||||||||||||||||||||||||||||||||||||||||

#ifndef MALKOHA_APP_HPP
#define MALKOHA_APP_HPP

//|||||||||||||||||||||||||||||||||||||||||||||||

#include "AdvancedOgreFramework.hpp"
#include "AppStateManager.hpp"

#include "MenuState.hpp"
#include "GameState.hpp"
#include "PauseState.hpp"

#include <SFML/System.hpp>

//|||||||||||||||||||||||||||||||||||||||||||||||

class MalkohaApp
{
public:
	MalkohaApp();
	~MalkohaApp();

	void go();

private:
	AppStateManager*	m_pAppStateManager;
	sf::Thread* mLoop;
	sf::Thread* mAsyncLoop;
};

//|||||||||||||||||||||||||||||||||||||||||||||||

#endif

//|||||||||||||||||||||||||||||||||||||||||||||||