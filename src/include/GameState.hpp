//|||||||||||||||||||||||||||||||||||||||||||||||

#ifndef GAME_STATE_HPP
#define GAME_STATE_HPP

//|||||||||||||||||||||||||||||||||||||||||||||||

#include "AppState.hpp"
#include "Avatar.hpp"
#include <MalkohaCamera.hpp>
#include "Ant.hpp"

#include "Tutorial.hpp"
#include <OgreSubEntity.h>
#include <OgreMaterialManager.h>
#include "level.h"
#include <CollisionTools.h>


#define COIN_PERIOD 2


//|||||||||||||||||||||||||||||||||||||||||||||||

class GameState : public AppState
{
public:
	GameState();
	void destroy();
	DECLARE_APPSTATE_CLASS(GameState);

	void enter();
	void createScene();
	void exit();
	bool pause();
	void resume();

	bool keyPressed(const OIS::KeyEvent &keyEventRef);
	bool keyReleased(const OIS::KeyEvent &keyEventRef);

	bool mouseMoved(const OIS::MouseEvent &arg);
	bool mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
	bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id);

	void onLeftPressed(const OIS::MouseEvent &evt);

	void update(double timeSinceLastFrame);

	void setLevelName(Ogre::String levelName);
	void setType(BEAT beat);
	void setCom(Ogre::String com);
	void setNextLevel(Ogre::String nextLevel);
	void setDataToWrite(Ogre::String name, bool write);
private:
	bool					m_bQuit;
	Avatar*					m_pChara;
	MalkohaCamera*			m_pMalCamera;
	LevelReader*			m_pLevelReader;
	std::vector<int>		m_pCheckpoints;
	std::vector<Ant*>		m_pAntList;
	std::vector<Ogre::Vector2> m_pAntPoint;
	Ogre::String			m_pLevelName;
	Ogre::String			m_pWriteDataName;
	bool					mWrite;
	BEAT					mBeatType;
	Ogre::String			mCom;
	int end;
	Ogre::String mNextLevel;
	std::vector<Tutorial*> tutorial;
};

//|||||||||||||||||||||||||||||||||||||||||||||||

#endif

//|||||||||||||||||||||||||||||||||||||||||||||||
