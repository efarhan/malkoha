#ifndef TUTORIAL_HPP
#define TUTORIAL_HPP
#include <Ogre.h>
#include <AdvancedOgreFramework.hpp>

class Tutorial
{
public:
	Tutorial(){dead = false;};
	~Tutorial(){};

	Ogre::String text;
	bool dead;
	int pos;
	void execute(bool* begin);
};

#endif