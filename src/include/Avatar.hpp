#ifndef AVATAR_HPP
#define AVATAR_HPP
#include "Ogre.h"
#include "OIS.h"
#include <hid.h>
#include "serial.h"
#include "CollisionTools.h"
#include <ctime>
#include <SFML/Audio.hpp>
#include <SFML/System.hpp>
#include <Ant.hpp>

#define CAM_HEIGHT 10
#define CHAR_HEIGHT 5  
#define RUN_SPEED 10
#define ACCELERATION 5
#define TURN_SPEED 500.0f
#define SIDE_SPEED 20
#define SIDE_LIMIT 6
#define SIDE_MARGE 3
#define ALPHA_DURATION 300.0
#define GLOW_HZ 1

typedef enum {
	WALK,
	FALLBACK,
	DOWNSWIPE,
	IDLE,
	ANIM_LENGTH,
} ANIM_INDEX;
typedef enum
{
	NONE,
	HZ,
	SYNC,
	ASYNC,
} BEAT;
class  Avatar
{
public:
	Avatar(Ogre::String name, Ogre::SceneManager* sceneMgr);
	~Avatar();
	void update(Ogre::Real deltatime);
	void injectKeyDown(const OIS::KeyEvent& evt);
	void injectKeyUp(const OIS::KeyEvent& evt);
	void injectMouseMove(const OIS::MouseEvent& evt);
	void injectMouseDown(const OIS::MouseEvent& evt, OIS::MouseButtonID id);

	void setCheckpoints(std::vector<int>* checkpoints);

	Ogre::SceneNode *getSightNode();
	Ogre::SceneNode *getCameraNode();
	Ogre::Vector3 getWorldPosition();

	void enterQTEMode(Ant* currentAnt);

	void setType(BEAT beat);

	sf::Music music;
	bool QTEmode;

	void setupAll(Ogre::SceneManager* sceneMgr);
	void setCOM(Ogre::String com);
	bool begin;
	Ogre::String mDataWrite;
	bool write;
	bool start;
	FILE* writeFile;
	int score;
private:
	void setupBody(Ogre::SceneManager* sceneMgr);
	void setupIllumination(Ogre::SceneManager* sceneMgr);
	void setupAnimations();
	void updateBody(Ogre::Real deltaTime);
	void updateAnimations(Ogre::Real deltaTime);
	void updateIllumination(Ogre::Real deltaTime);
	void updateKeys();

	void updateScore();

	Ogre::Entity* mBodyEnt;
	Ogre::Vector3 mKeyDirection;      // player's local intended direction based on WASD keys
	Ogre::AnimationState* mAnims[ANIM_LENGTH];
	int mAnimIndex;

	Ogre::String mName;
	Ogre::SceneNode *mMainNode; // Main character node
	Ogre::SceneNode *mSightNode; // "Sight" node - The character is supposed to be looking here
	Ogre::SceneNode *mCameraNode; // Node for the chase camera
	Ogre::SceneManager *mSceneMgr;

	Ogre::BillboardSet* mGlowSet;
	Ogre::Billboard* mGlow;

	Ogre::Vector3 mOldPos;
	MOC::CollisionTools* collisionTool;

	bool isConnected;
	Ogre::Real timeGlow1Hz;
	bool beat;
	std::clock_t bpmtime_sync;
	float lastX;
	float alphaGlow;
	ArduinoData mArduinoData;
	ArduinoData mAsyncData;

	float mHeight;
	int collisionTest;

	sf::SoundBuffer coinBuffer;
	sf::Sound coin;
	sf::SoundBuffer screamBuffer;
	sf::Sound scream;
	sf::SoundBuffer antBuffer;
	sf::Sound ant;
	sf::SoundBuffer splashBuffer;
	sf::Sound splash;

	




	std::vector<int>* mCheckpoints;

	//QTE
	bool attack;
	Ant* mCurrentAnt;
	
	BEAT beat_type;

	Ogre::String mCom;


};
#endif
