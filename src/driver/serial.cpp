




#include "serial.h"




static sf::Mutex dataMutex;
static sf::Mutex aDataMutex;
static sf::Mutex deviceMutex;
static sf::Mutex fileMutex;

static volatile bool running=true;
static volatile bool asyncRunning=false;
static volatile bool opened=false;
static volatile int threat = 0;
static FILE *dataFile = NULL;
static bool textMode = false;

static clock_t fileStartTime;
static clock_t startTime = clock();

static ArduinoData data;
static ASyncData *aData;


#ifdef TEST_FILE
int writeCount=0;


#endif




#if (1000%CLOCKS_PER_SEC != 0) //just in case we wanna run it on some strange system...
	#define getTimeMS(x) ((1000*((uint64_t)(clock()-(x))))/CLOCKS_PER_SEC)
#else
	#define getTimeMS(x) (PERIOD*((uint64_t)(clock()-(x))))
#endif
#define getTime(x) (getTimeMS(x)/1000)





#ifdef _WIN32

static HANDLE serialPort;
#define WAIT(x) Sleep(x)
#else
//you can add some posix stuff here, but for now only windows is supported.
#endif

int test() {
	
	return 0;
}

int closeFile() {
	int status = 0;
	if (dataFile) {
		fileMutex.lock();
		status = fclose(dataFile);
		dataFile = NULL;
		fileMutex.unlock();
	}
	return status;
}

int close() {
	running = false;
	return closeFile();
}
void writeData(float ecg, int beatBit, float conductance) {
	int threatValue;
	ArduinoData d = { ecg, beatBit, conductance };
	dataMutex.lock();
	data = d;
	threatValue = threat;
	dataMutex.unlock();

#ifndef TEST_FILE
	if (dataFile) {
		fileMutex.lock();
		FileData fData = { d, getTimeMS(fileStartTime), threatValue };
		writeToFile(dataFile, &fData,textMode);
		fileMutex.unlock();
	}
#endif
}
void writeToFile(FILE *f, FileData *d, bool text) {
	if (text) {
		fprintf(f,"%u,%d,%f,%d,%f\n",d->time,d->threat,d->data.ecg,d->data.beat,d->data.conductance);
	}
	else {
		ConstantSize_FileData fData = { toInteger(d->data.ecg), d->data.beat, toInteger(d->data.conductance), d->time, d->threat };
		fwrite(&fData,sizeof(ConstantSize_FileData),1,f);
	}
	fflush(f);
#ifdef TEST_FILE
	printf("Writing #%d completed!\n",writeCount++);
#endif
}
int readFromFile(FILE *f, FileData *d, bool text) {
	if (text) {
		return 5!=fscanf(f,"%u,%d,%f,%d,%f\n",&d->time,&d->threat,&d->data.ecg,&d->data.beat,&d->data.conductance);
	}
	else {
		ConstantSize_FileData fData;
		size_t read = fread(&fData,sizeof(ConstantSize_FileData),1,f);
		if (read == 1) {
			d->data.ecg = toFloat(fData.ecg);
			d->data.beat = fData.beat;
			d->data.conductance = toFloat(fData.conductance);
			d->threat = fData.threat;
			d->time = fData.time;
			return 0;
		}
		else
			return -1;
	}
}
int convertFile(const char *input, const char *output, bool binaryToTxt) {
	int status=0;
	FILE *in,*out;

	in = fopen(input,binaryToTxt?"rb":"r");
	out = fopen(output,binaryToTxt?"w":"wb");

	if (in&&out) {
		FileData buffer;
		while(!readFromFile(in,&buffer,!binaryToTxt)) {
			writeToFile(out,&buffer,binaryToTxt);
		}
		fclose(in);
		fclose(out);
	}
	else {
		status = errno;
	}
	return status;
}
void initData() {
	writeData(0,false,0);
}
void readData(ArduinoData *normalData, ArduinoData *asyncData) {
	dataMutex.lock();
	*normalData=data;
	dataMutex.unlock();
	aDataMutex.lock();
	if (aData) {
		*asyncData=aData->data;
	}
	aDataMutex.unlock();
}
void setThreat(int value) {
	dataMutex.lock();
	threat = value;
	dataMutex.unlock();
}
void writeData(const char *rawData) {
	float ecg,conductance;
	int beatBit;
	int read=sscanf(rawData," %f , %d , %f ",&ecg,&beatBit,&conductance);

	if (read==3) {
		writeData(ecg,beatBit,conductance);
	}
	else {
	}
}
int closeSerial() {
	if (opened) {
		deviceMutex.lock();
		CloseHandle(serialPort);
		opened=false;
		initData();
		deviceMutex.unlock();
	}
	return 0;
}

int openFile(const char *fileName, bool append, bool text) {
	int status=0;
	char accessMode[3];
	sprintf(accessMode,"%c%s",append?'a':'w',text?"":"b");
	closeFile(); //closeFile has its own set of lock/unlock, so we leave it out of this one
	fileMutex.lock();
	textMode=text;
	dataFile = fopen(fileName,accessMode);
	if (!dataFile) {
		status=errno;
	}
	else {
		fileStartTime = clock();
		//fileOpened = clock();
	}
	fileMutex.unlock();
	return status;
}

int freeAllElements(ASyncData *elem) {
	ASyncData *current=NULL;
	ASyncData *next = elem;
	while(next) {
		current=next;
		next = current->next;
		free(current);
	}
	return 0;
}

int openAsyncDataFile(const char *fileName, bool text) {
	int status=0;
	FILE *f;

	if (asyncRunning) {
		closeAsync();
		WAIT(LOOP_WAIT_TIME*2); //makes sure the loop had time to exit
	}
	

	f = fopen(fileName,text?"r":"rb");

	if (f) {
		int lastBeat=-1;
		FileData buffer;
		ASyncData *asyncBuffer = NULL;
		ASyncData *last = NULL;
		ASyncData *beforeLastBeat = NULL;
		aDataMutex.lock();
		asyncRunning=true;
		
		bool prout=true;
		while(!readFromFile(f,&buffer,text)) {
			if (buffer.data.beat) {
				if (lastBeat > MAX_BEAT_SKIP) {
					beforeLastBeat = last;
				}
				if (prout) {
					prout = false;
				}
				lastBeat=0;
			}
			else {
				if (lastBeat>=0)
					lastBeat++;
			}
			if (lastBeat>=0) {
				asyncBuffer = (ASyncData*)malloc(sizeof(ASyncData));
				if (!asyncBuffer) {
					//TODO do some stuff to handle the error (free memory, etc...)
				}
				else {
					asyncBuffer->data = buffer.data;
					asyncBuffer->time = buffer.time;
					asyncBuffer->next = NULL;
					if (last) {
						last->next = asyncBuffer;
					}
					else {
						aData = asyncBuffer;
					}
					last = asyncBuffer;
				}
			}
		}
		
		if (beforeLastBeat) {
			freeAllElements(beforeLastBeat->next);
			beforeLastBeat->next = aData; //looped list
		}
		else {
			status=-1; //not enough data for a loop!
		}
		aDataMutex.unlock();
		fclose(f);
	}
	else {
		status = errno;
	}
	return status;
}

int closeAsync() {
	//free the looped list
	aDataMutex.lock();
	asyncRunning=false;
	if (!aData) {} //nothing to do
	else if (!aData->next) {
		free(aData);
		aData = NULL;
		return 0;
	}
	else {
		ASyncData *next = aData->next;
		aData->next = NULL; //break the loop, so freeAllElements won't segfault
		aData = NULL;
		freeAllElements(next);
	}
	
	aDataMutex.unlock();
	return 0;
}

#define getAsyncTime() (getTimeMS(startTime)-offset)

int aSyncMainLoop() {

	uint64_t offset=0;
	uint64_t next=0;

	asyncRunning=true;
	aDataMutex.lock();

	//TODO something to handle the case when we close while the thread is still waiting for aData; in the current state the thread will stay alive forever, or until we start another loop, and then get in the way...
	while(asyncRunning&&!aData) {
		aDataMutex.unlock();
		WAIT(LOOP_WAIT_TIME);
		aDataMutex.lock();
	}
	if (aData) {
		next=aData->time;
	
		aDataMutex.unlock();

		offset=getTimeMS(startTime)-next;


		while(asyncRunning) {

			if(next<=getAsyncTime()) {
				aDataMutex.lock();
				if (aData) {
					aData = aData->next;
					if (aData->time < next) {
						offset=getTimeMS(startTime)-aData->time;
					}
					next = aData->time;
				}
				aDataMutex.unlock();
			}

			WAIT(LOOP_WAIT_TIME);
		}

	}

	return 0;
}


int openSerial(const char *comName) {
	
#ifdef _WIN32
	DCB dcb;
	//char *comName = "COM5";

	closeSerial();
	

	
	serialPort = CreateFile( comName,
					GENERIC_READ | GENERIC_WRITE,
					0,    // must be opened with exclusive-access
					NULL, // no security attributes
					OPEN_EXISTING, // must use OPEN_EXISTING
					0,    // not overlapped I/O
					NULL  // hTemplate must be NULL for comm devices
					);
	if (serialPort == INVALID_HANDLE_VALUE) {
		return COM_NOT_FOUND;
	}

	if (!GetCommState(serialPort, &dcb)) {
		return OPEN_FAILED;
	}

	dcb.BaudRate = CBR_115200;
	dcb.ByteSize = 8;
	dcb.Parity = NOPARITY;
	dcb.StopBits = ONESTOPBIT;

	if (!SetCommState(serialPort, &dcb)) {
		return CONFIGURE_FAILED;
	}


	opened = true;


	return 0;
#else
	return UNSUPPORTED;
#endif
}

/*typedef enum {	INIT=0,
				READING_ECG_1,
				READING_ECG_2,
				READING_BEAT_1,
				READING_BEAT_2,
				READING_CONDUCT_1,
				READING_CONDUCT_2
} State;*/

int readSerial() {
	

	char singleChar;
	char buffer[BUFFER_SIZE];
	int i=0;

#ifdef _WIN32
	DWORD byteRead;
#else
	int byteRead;
#endif

	int state = 0;

#ifdef TEST_FILE
	FileData fData = { { 123.456, 0, 78.9 }, getTimeMS(fileStartTime), 0 };
	fileMutex.lock();
	writeToFile(dataFile, &fData,textMode);
	fileMutex.unlock();

#endif

	deviceMutex.lock();


	if (!opened) {
		deviceMutex.unlock();
		WAIT(LOOP_WAIT_TIME); //so we don't overload CPU when we have no serial
		return 0;

	}
	while(state<2) {

	#ifdef _WIN32
		int status = ReadFile(serialPort,&singleChar,1,&byteRead,NULL);
		if(!status)
		{
			status = GetLastError();
		}
		else
		{
			status = 0;
		}
	#else
		byteRead=1;
		singleChar=' ';
	#endif
		
		switch(state) {
		case 0: //waiting for [
			if (singleChar == '[')
				++state;
			break;
		case 1:
			if (singleChar == ']')
				++state;
			else
				buffer[i++]=singleChar;
			break;
		}
		if (i>=BUFFER_SIZE-1) {
			buffer[i]='\0';
			deviceMutex.unlock();
			return BUFFER_OVERFLOW;
		}
	}
	deviceMutex.unlock();
	buffer[i]='\0';
	writeData(buffer);



	return 0;
}

void printData() {
	ArduinoData d;
	ArduinoData asyncData;
	readData(&d,&asyncData);
}

int serialMainLoop() {
	initData();
	running = true;
	while(running) {
		readSerial();
		//WAIT(LOOP_WAIT_TIME);
		//printData();
	}

	return 0;
}


