#include "Avatar.hpp"
#include "AdvancedOgreFramework.hpp"
Avatar::Avatar(Ogre::String name, Ogre::SceneManager* sceneMgr) : bpmtime_sync(0)
{

	mName = name;
	mSceneMgr = sceneMgr;
	OgreFramework::getSingletonPtr()->m_pLog->logMessage("Setting up nodes...");
	if(mSceneMgr == NULL)
	{
		OgreFramework::getSingletonPtr()->m_pLog->logMessage("SceneManager not ok...");
		
	}
	
	mMainNode = mSceneMgr->getRootSceneNode()->createChildSceneNode(mName);
	OgreFramework::getSingletonPtr()->m_pLog->logMessage("Main node done...");
	mSightNode = mMainNode->createChildSceneNode (mName + "_sight", Ogre::Vector3 (-50, 0, 0));
	OgreFramework::getSingletonPtr()->m_pLog->logMessage("Sight node done...");
	mCameraNode = mMainNode->createChildSceneNode (mName + "_camera", Ogre::Vector3 (10, CAM_HEIGHT, 0), Ogre::Quaternion::IDENTITY);
	OgreFramework::getSingletonPtr()->m_pLog->logMessage("Camera node done...");

	OgreFramework::getSingletonPtr()->m_pLog->logMessage("Setting up body...");
	
	

	alphaGlow = 0.0;
	lastX = Ogre::Math::PI;

	attack = false;
	begin = true;
	start = false;
	score = 0;

	QTEmode = false;
	mCurrentAnt = NULL;

	//Loading sound
	if(coinBuffer.loadFromFile("media/sound/coin-drop-1.wav"))
	{
		OgreFramework::getSingletonPtr()->m_pLog->logMessage("Coin sound successfully loded");
		coin.setBuffer(coinBuffer);
	}
	if(screamBuffer.loadFromFile("media/sound/man-scream-ahh-01.wav"))
	{
		OgreFramework::getSingletonPtr()->m_pLog->logMessage("Scream sound successfully loded");
		scream.setBuffer(screamBuffer);
	}
	if(antBuffer.loadFromFile("media/sound/spider.wav"))
	{
		OgreFramework::getSingletonPtr()->m_pLog->logMessage("Ant sound successfully loded");
		ant.setBuffer(antBuffer);
	}
	if(splashBuffer.loadFromFile("media/sound/splash.wav"))
	{
		OgreFramework::getSingletonPtr()->m_pLog->logMessage("Ant sound2 successfully loded");
		splash.setBuffer(splashBuffer);
	}
	//loading music
	if(music.openFromFile("media/sound/justin.ogg"))
	{
		OgreFramework::getSingletonPtr()->m_pLog->logMessage("Music successfully loded");
		music.play();
	}
}
Avatar::~Avatar()
{
	//End writing
	closeFile();

	delete collisionTool;
}
void Avatar::setupAll(Ogre::SceneManager* sceneMgr)
{
	setupBody(sceneMgr);

	setupIllumination(sceneMgr);
	setupAnimations();

	collisionTool = new MOC::CollisionTools(mSceneMgr);

	
		
	Ogre::AxisAlignedBox aab = mBodyEnt->getBoundingBox();
	mHeight = aab.getSize().y * (1.0f - Ogre::MeshManager::getSingleton().getBoundsPaddingFactor());
	collisionTest = (int)mHeight;
}
void Avatar::update(Ogre::Real deltaTime)
{
	updateBody(deltaTime);
	updateAnimations(deltaTime);
	updateKeys();
	updateIllumination(deltaTime);
}
void Avatar::setupBody(Ogre::SceneManager* sceneMgr)
{
	mBodyEnt= sceneMgr->createEntity("NinjaEntity", "ninja.ms3d.m.mesh");
	mMainNode->attachObject(mBodyEnt);
	mKeyDirection = Ogre::Vector3::ZERO;
}
void Avatar::setupIllumination(Ogre::SceneManager* sceneMgr)
{
	mGlowSet = mSceneMgr->createBillboardSet("Glows");
	mGlowSet->setMaterialName("Malkoha/Glow");
	mGlow = mGlowSet->createBillboard(Ogre::Vector3(0, 2, 0));
	mGlow->setColour(Ogre::ColourValue(1, 1, 1, 0));
	mGlow->setDimensions(30, 30);
	mMainNode->attachObject(mGlowSet);
	if(mGlow->getColour().a == 0)
		OgreFramework::getSingletonPtr()->m_pLog->logMessage("Glow alpha is 0");
	isConnected = false;
	timeGlow1Hz = 0.0f;
}

void Avatar::setupAnimations()
{
	//mBodyEnt->getSkeleton()->setBlendMode(Ogre::ANIMBLEND_CUMULATIVE);
	Ogre::String animName = "WALK";
	mAnims[WALK] = mBodyEnt->getAnimationState(animName);
	mAnims[WALK]->setLoop(true);
	mAnims[WALK]->setEnabled(true);
	
/*
	animName = "STEALTH";
	mAnims[1] = mBodyEnt->getAnimationState(animName);
	mAnims[1]->setLoop(true);
	*/
	animName = "FALLBACK";
	mAnims[FALLBACK] = mBodyEnt->getAnimationState(animName);
	mAnims[FALLBACK]->setLoop(false);
	mAnims[FALLBACK]->setEnabled(false);

	animName = "DOWNSWIPE";
	mAnims[DOWNSWIPE] = mBodyEnt->getAnimationState(animName);
	mAnims[DOWNSWIPE]->setLoop(false);
	mAnims[DOWNSWIPE]->setEnabled(false);

	animName = "IDLE2";
	mAnims[IDLE] = mBodyEnt->getAnimationState(animName);
	mAnims[IDLE]->setLoop(true);
	mAnims[IDLE]->setEnabled(false);

	mAnimIndex = WALK;
}
void Avatar::updateBody(Ogre::Real deltaTime)
{
	if(mAnimIndex == WALK)
	{
		if(begin)
		{
			mMainNode->translate (mMainNode->_getDerivedOrientation() * 
				Ogre::Vector3(-RUN_SPEED * deltaTime / 1000 
					- (mKeyDirection.z)* ACCELERATION * deltaTime / 1000,
					0, SIDE_SPEED * deltaTime * mKeyDirection.x/1000));
		}
		else
		{
			mMainNode->translate (0,0, SIDE_SPEED * deltaTime * mKeyDirection.x/1000);
		}
	}
	if(mMainNode->_getDerivedPosition().z > SIDE_LIMIT)
	{
		mMainNode->_setDerivedPosition(Ogre::Vector3(
			mMainNode->_getDerivedPosition().x,
			mMainNode->_getDerivedPosition().y,
			SIDE_LIMIT));
	}
	else if(mMainNode->_getDerivedPosition().z < -SIDE_LIMIT)
	{
		mMainNode->_setDerivedPosition(Ogre::Vector3(
			mMainNode->_getDerivedPosition().x,
			mMainNode->_getDerivedPosition().y,
			-SIDE_LIMIT));
	}
	//mMainNode->yaw (Ogre::Radian (deltaTime/1000 * mKeyDirection.x));
}
void Avatar::updateKeys()
{
	Data* newKey = getData();
	if(newKey != NULL)
	{
		float x = -newKey->x;
		if(x > 0.2 || x < -0.2)
		{
			mKeyDirection.x = x;
		}
		else
		{
			mKeyDirection.x = 0;
		}
		if(newKey->button)
		{
			OgreFramework::getSingletonPtr()->m_pLog->logMessage("KEY ATTACK");
			OgreFramework::getSingletonPtr()->m_pTrayMgr->removeWidgetFromTray("TutoLbl");
			attack = true;
			begin = true;
			if(QTEmode)
			{
				OgreFramework::getSingletonPtr()->m_pLog->logMessage("KILL THE ANT");
				mCurrentAnt->die();
				//update score
				score+=5;
				updateScore();
				QTEmode = false;
				splash.play();
			}
			if(!start && write)
			{
				//start writing
				openFile(mDataWrite.c_str(), false, true);
				start = true;
			}
		}
	}
}
void Avatar::updateIllumination(Ogre::Real deltaTime)
{
	if(beat_type != HZ && beat_type != NONE)
	{
		readData(&mArduinoData, &mAsyncData);
		if(beat_type == ASYNC)
		{
			beat = mAsyncData.beat;
		}
		else if(beat_type == SYNC)
		{
			beat = mArduinoData.beat;
		}
	}
	else if (beat_type == HZ)
	{

		if(timeGlow1Hz < GLOW_HZ)
		{
			timeGlow1Hz += deltaTime/1000;
			beat = false;
		}
		else
		{
			timeGlow1Hz = 0;
			beat = true;
		}
	}
	Ogre::ColourValue oldColour = mGlow->getColour();
	std::clock_t now = clock();
	float period = 0.0;
	if(beat)
	{
		
		std::clock_t tmp = now;
		period = ((float)(tmp-bpmtime_sync))/CLOCKS_PER_SEC;
		beat = (period > 0.3) && (period < 2.0);
	bpmtime_sync = tmp;
	}
	if(beat)
	{
		lastX = 0.0;
	}
	else
	{
		if(lastX < Ogre::Math::PI)
		{
			lastX += deltaTime / ALPHA_DURATION * Ogre::Math::PI;
			alphaGlow = 0.5*Ogre::Math::Sin(lastX);
		}
		else
		{
			alphaGlow = 0.0;
		}
	}
	mGlow->setColour(Ogre::ColourValue(oldColour.r, oldColour.g, oldColour.b, alphaGlow));
}
void Avatar::injectKeyDown(const OIS::KeyEvent& evt)
{

	if (evt.key == OIS::KC_W || evt.key == OIS::KC_UP) mKeyDirection.z = 1;
	else if (evt.key == OIS::KC_A || evt.key == OIS::KC_LEFT) mKeyDirection.x = 1;
	else if (evt.key == OIS::KC_S || evt.key == OIS::KC_DOWN) mKeyDirection.z = -1;
	else if (evt.key == OIS::KC_D || evt.key == OIS::KC_RIGHT) mKeyDirection.x = -1;
	else if (evt.key == OIS::KC_SPACE || evt.key == OIS::KC_RETURN || evt.key == OIS::KC_RCONTROL)
	{
		OgreFramework::getSingletonPtr()->m_pTrayMgr->removeWidgetFromTray("TutoLbl");
		attack = true;
		begin = true;
		if(QTEmode)
		{
			OgreFramework::getSingletonPtr()->m_pLog->logMessage("KILL THE ANT");
			mCurrentAnt->die();
			//update score
			score++;
			updateScore();
			QTEmode = false;
			splash.play();
			//TODO stop threat
		}
		if(!start && write)
		{
			//start writing
			openFile(mDataWrite.c_str(), false, true);
			start = true;
		}
	}
}

void Avatar::injectKeyUp(const OIS::KeyEvent& evt)
{
	// keep track of the player's intended direction
	if ((evt.key == OIS::KC_W || evt.key == OIS::KC_U) && mKeyDirection.z ==1) mKeyDirection.z = 0;
	else if ((evt.key == OIS::KC_A || evt.key == OIS::KC_LEFT) && mKeyDirection.x == 1) mKeyDirection.x = 0;
	else if ((evt.key == OIS::KC_S || evt.key == OIS::KC_DOWN) && mKeyDirection.z == -1) mKeyDirection.z = 0;
	else if ((evt.key == OIS::KC_D || evt.key == OIS::KC_RIGHT) && mKeyDirection.x == -1) mKeyDirection.x = 0;
}
void Avatar::injectMouseMove(const OIS::MouseEvent& evt)
{
	// update camera goal based on mouse movement
	//updateCameraGoal(-0.05f * evt.state.X.rel, -0.05f * evt.state.Y.rel, -0.0005f * evt.state.Z.rel);
}

void Avatar::injectMouseDown(const OIS::MouseEvent& evt, OIS::MouseButtonID id)
{
}
void Avatar::updateAnimations(Ogre::Real deltaTime)
{
	mAnims[mAnimIndex]->setEnabled(false);
	if(mAnimIndex == WALK)
	{
		//collisionTool->calculateY(mMainNode,true,true,2.0f,1<<8);
		

		Ogre::String object = "";
		bool col = false;
		float distToColl = 0.0f;
		float collisionRadius = 2.5f;
		Ogre::MovableObject* myObject = NULL;
		float distToDest = 0.0f;
		for(int i = 0; i < collisionTest; i++)
		{
			
			Ogre::Vector3 pos = mMainNode->getPosition();
			Ogre::Vector3 toPos = pos + mMainNode->getOrientation()*Ogre::Vector3::UNIT_SCALE;
			float rayHeightLevel = i;


			Ogre::Vector3 fromPointAdj(pos.x, pos.y + rayHeightLevel, pos.z);
			Ogre::Vector3 toPointAdj(toPos.x, toPos.y + rayHeightLevel, toPos.z);
			Ogre::Vector3 normal = toPointAdj - fromPointAdj;
			distToDest = normal.normalise();
			Ogre::Vector3 myResult(0, 0, 0);


			if( collisionTool->raycastFromPoint(fromPointAdj, normal, myResult, myObject, distToColl))
			{
				object = myObject->getName();
				if(object != "WallLeftEntity" && object != "NinjaEntity")
				{
					col = true;
					break;
				}
			}

		}


		if (col)
		{
			OgreFramework::getSingletonPtr()->m_pLog->logMessage(Ogre::String("Collision with : ")+object);
			distToColl -= collisionRadius;
			if(distToColl <= distToDest)
			{
				if(object.find("Machine") != Ogre::String::npos)
				{
					OgreFramework::getSingletonPtr()->m_pLog->logMessage("Collision with Machine");
					mAnimIndex = FALLBACK;
					mAnims[mAnimIndex]->setTimePosition(0);
					scream.play();
					score--;
					updateScore();
				}
				else if(object.find("Coin") != Ogre::String::npos)
				{
					Ogre::SceneNode* parent = myObject->getParentSceneNode();
					parent->detachObject(myObject);
					//mSceneMgr->destroySceneNode(parent->getName());

					OgreFramework::getSingletonPtr()->m_pLog->logMessage("Collision with Coin");
					//add score to player, make a sound
					score+=2;
					updateScore();
					coin.play();
				}
			}
		}
		else
		{
			if(attack)
			{
				mAnimIndex = DOWNSWIPE;
				mAnims[mAnimIndex]->setTimePosition(0);
			}
		}
	}
	else
	{
		if(QTEmode && !attack)
		{
			if(mCurrentAnt->addQTETime(deltaTime))
			{
				setThreat(0);
				OgreFramework::getSingletonPtr()->m_pLog->logMessage("QTE FAIL");
				mAnimIndex = FALLBACK;
				mAnims[mAnimIndex]->setTimePosition(0);
				scream.play();
				score--;
				updateScore();
				QTEmode = false;
				mMainNode->translate(10,0,0);
			}
		}

		if(mAnimIndex == DOWNSWIPE && mAnims[mAnimIndex]->hasEnded())
		{
			OgreFramework::getSingletonPtr()->m_pLog->logMessage("End of Downswipe");
			attack = false;
			mAnimIndex = WALK;
			mAnims[mAnimIndex]->setTimePosition(0);
		}
		else if (attack && (mAnimIndex != DOWNSWIPE && mAnimIndex != FALLBACK))
		{
			OgreFramework::getSingletonPtr()->m_pLog->logMessage("Attack animation");
			mAnimIndex = DOWNSWIPE;
			mAnims[mAnimIndex]->setTimePosition(0);
		}

		if(mAnimIndex == FALLBACK && mAnims[mAnimIndex]->hasEnded())
		{
			OgreFramework::getSingletonPtr()->m_pLog->logMessage("End of Fallback");
			
			//move to last checkpoints
			if(mCheckpoints != NULL)
			{
				std::vector<int>::iterator it;
				int pos = mMainNode->getPosition().x;
				int newPos = 0;
				for(it = mCheckpoints->begin(); it != mCheckpoints->end(); ++it)
				{
					if((*it) > pos && newPos > (*it))
					{
						newPos = *it;
					}
				}
				mMainNode->translate (mMainNode->getOrientation() * Ogre::Vector3(newPos-pos,0,0));
			}
			else
			{
				mMainNode->translate (mMainNode->getOrientation() * Ogre::Vector3(100,0,0));
			}
			mAnimIndex = WALK;
		}
	}
	mAnims[mAnimIndex]->setEnabled(true);
	float coeff = 1.0f;
	if(mAnimIndex == WALK)
	{
		coeff = -0.3f*mKeyDirection.z*mKeyDirection.z+0.5f * mKeyDirection.z+1;
	}
	if(begin)
		mAnims[mAnimIndex]->addTime(coeff*deltaTime / 1000);
}

Ogre::SceneNode* Avatar::getSightNode()
{
	return mSightNode;
}
Ogre::SceneNode* Avatar::getCameraNode()
{
	return mCameraNode;
}
Ogre::Vector3 Avatar::getWorldPosition()
{
	return mMainNode->getPosition();
}

void Avatar::setCheckpoints(std::vector<int>* checkpoints)
{
	mCheckpoints = checkpoints;
}

void Avatar::updateScore()
{
	char newText [50];
	sprintf(newText, "Score: %d", score);
	((OgreBites::Label*)OgreFramework::getSingletonPtr()->m_pTrayMgr->getWidget("ScoreLbl"))->setCaption(newText);
}
void Avatar::enterQTEMode(Ant* currentAnt)
{
	//set threat true in file
	if(write)
	{
		setThreat(1);
	}
	OgreFramework::getSingletonPtr()->m_pLog->logMessage("Enter QTEMode");
	mCurrentAnt = currentAnt;
	mCurrentAnt->enterQTEMode();
	mAnimIndex = IDLE;
	ant.play();
	QTEmode = true;
	attack = false;
}
void Avatar::setType(BEAT beat)
{
	beat_type = beat;

}
void Avatar::setCOM(Ogre::String com)
{
	mCom = com;
}