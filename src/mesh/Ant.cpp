#include <Ant.hpp>

Ant::Ant(Ogre::String name, Ogre::SceneManager* sceneMgr)
{
	mName = name;
	mSceneMgr = sceneMgr;
	dead = false;
	QTETime = QTE_TIME;
	timeToFall = TIME_TO_FALL;
	mAnimIndex = -1;
	

}
Ant::~Ant()
{

}
void Ant::setupAnimation()
{
	Ogre::String animName = "WALK";
	mAnims[ANT_WALK] = mEntity->getAnimationState(animName);
	mAnims[ANT_WALK]->setLoop(true);
	mAnims[ANT_WALK]->setEnabled(false);

	animName = "STUN";
	mAnims[ANT_STILL] = mEntity->getAnimationState(animName);
	mAnims[ANT_STILL]->setLoop(true);
	mAnims[ANT_STILL]->setEnabled(false);

	animName = "ATTACK";
	mAnims[ANT_ATTACK] = mEntity->getAnimationState(animName);
	mAnims[ANT_ATTACK]->setLoop(false);
	mAnims[ANT_ATTACK]->setEnabled(false);

	mAnimIndex = -1;
}
void Ant::update(Ogre::Real deltatime)
{
	if(mAnimIndex != -1)
	{

		mAnims[mAnimIndex]->addTime(deltatime/1000);
		
	}
	if(timeToFall > 0)
	{
		mNode->translate(0, deltatime/TIME_TO_FALL*(5-y), 0);
		timeToFall-=deltatime;
		if(mNode->getPosition().y < 0)
		{
			mNode->setPosition(mNode->getPosition().x, 0, mNode->getPosition().z);
			timeToFall = -1;
		}
	}
}

void Ant::setNode(Ogre::SceneNode* node)
{
	mNode = node;
	mNode->rotate(Ogre::Vector3(1, 0, 0), Ogre::Radian(Ogre::Math::PI), Ogre::Node::TS_WORLD);
	y = mNode->getPosition().y;
}
void Ant::setEntity(Ogre::Entity* ent)
{
	mEntity = ent;
}
void Ant::enterQTEMode()
{
	mNode->rotate(Ogre::Vector3(1, 0, 0), Ogre::Radian(Ogre::Math::PI), Ogre::Node::TS_WORLD);
	mNode->rotate(Ogre::Vector3(0, 0, 1), Ogre::Radian(Ogre::Math::PI/4), Ogre::Node::TS_WORLD);
	mAnimIndex = ANT_STILL;
	mNode->translate(-10, 0, 0);
	timeToFall = TIME_TO_FALL;

	char buffer[100];
	sprintf(buffer,"Ant1 %f, %f, %f", mNode->getPosition().x, mNode->getPosition().y, mNode->getPosition().z);
	OgreFramework::getSingletonPtr()->m_pLog->logMessage(buffer);
}
void Ant::die()
{
	mSceneMgr->destroyEntity(mEntity);
	mNode->detachAllObjects();
	dead = true;
	setThreat(0);
}
bool Ant::addQTETime(Ogre::Real dTime)
{
	update(dTime);
	QTETime-=dTime;
	if(QTETime < 0)
	{
		QTETime = QTE_TIME;
		char buffer[100];
		//mNode->setPosition(mNode->getPosition().x, y, mNode->getPosition().z);
		mNode->rotate(Ogre::Vector3(0,0,1),Ogre::Radian(-Ogre::Math::PI/4),Ogre::Node::TS_WORLD);
		mNode->rotate(Ogre::Vector3(1,0,0),Ogre::Radian(-Ogre::Math::PI),Ogre::Node::TS_WORLD);
		mNode->translate(10, y-5, 0);
		timeToFall = TIME_TO_FALL;
		mAnims[mAnimIndex]->setEnabled(false);
		mAnimIndex = -1;
		return true;
	}
	else
	{
		mAnimIndex = ANT_STILL;
		mAnims[mAnimIndex]->setEnabled(true);
		return false;
	}

}
Ogre::Vector3 Ant::getPosition()
{
	return mNode->getPosition();
}