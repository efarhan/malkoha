#include "Tutorial.hpp"

void Tutorial::execute(bool* begin)
{
	*begin = false;
	((OgreBites::Label*)OgreFramework::getSingletonPtr()->m_pTrayMgr->getWidget("TutoLbl"))->setCaption(text);
	OgreFramework::getSingletonPtr()->m_pTrayMgr->moveWidgetToTray("TutoLbl", OgreBites::TL_CENTER);
}