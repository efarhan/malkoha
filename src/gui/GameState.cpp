//|||||||||||||||||||||||||||||||||||||||||||||||

#include "GameState.hpp"

//|||||||||||||||||||||||||||||||||||||||||||||||

using namespace Ogre;

//|||||||||||||||||||||||||||||||||||||||||||||||

GameState::GameState()
{
    m_bQuit             = false;
	m_pLevelReader = NULL;
}

void GameState::destroy()
{

	AppState::destroy();
}

//|||||||||||||||||||||||||||||||||||||||||||||||

void GameState::enter()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Entering GameState...");

    m_pSceneMgr = OgreFramework::getSingletonPtr()->m_pRoot->createSceneManager(ST_GENERIC, "GameSceneMgr");
	m_pCamera = m_pSceneMgr->createCamera("GameCamera");
	OgreFramework::getSingletonPtr()->m_pViewport->setCamera(m_pCamera);
	
	//GUI
	OgreFramework::getSingletonPtr()->m_pTrayMgr->hideCursor();
	OgreFramework::getSingletonPtr()->m_pTrayMgr->showLogo(OgreBites::TL_TOPLEFT);
	OgreFramework::getSingletonPtr()->m_pTrayMgr->showFrameStats(OgreBites::TL_BOTTOMLEFT);
	OgreFramework::getSingletonPtr()->m_pTrayMgr->createLabel(OgreBites::TL_TOPRIGHT, "ScoreLbl", "Score: 0", 250);
	OgreFramework::getSingletonPtr()->m_pTrayMgr->createLabel(OgreBites::TL_NONE, "TutoLbl", "", 500);
    
	createScene();
}

//|||||||||||||||||||||||||||||||||||||||||||||||

bool GameState::pause()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Pausing GameState...");
	m_pChara->music.stop();
    return true;
}

//|||||||||||||||||||||||||||||||||||||||||||||||

void GameState::resume()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Resuming GameState...");

    OgreFramework::getSingletonPtr()->m_pViewport->setCamera(m_pCamera);
	m_pChara->music.play();
    m_bQuit = false;
}

//|||||||||||||||||||||||||||||||||||||||||||||||

void GameState::exit()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Leaving GameState...");

    m_pSceneMgr->destroyCamera(m_pCamera);
	while(!m_pAntList.empty())
	{
		delete m_pAntList.back();
		m_pAntList.pop_back();
	}
	while(!tutorial.empty())
	{
		delete tutorial.back();
		tutorial.pop_back();
	}
	delete m_pChara;
	delete m_pMalCamera;
	delete m_pLevelReader;
	if(OgreFramework::getSingletonPtr()->m_pTrayMgr->getWidget("ScoreLbl"))
		OgreFramework::getSingletonPtr()->m_pTrayMgr->destroyWidget("ScoreLbl");
	if(OgreFramework::getSingletonPtr()->m_pTrayMgr->getWidget("TutoLbl"))
		OgreFramework::getSingletonPtr()->m_pTrayMgr->destroyWidget("TutoLbl");
    if(m_pSceneMgr)
        OgreFramework::getSingletonPtr()->m_pRoot->destroySceneManager(m_pSceneMgr);
}

//|||||||||||||||||||||||||||||||||||||||||||||||

void GameState::createScene()
{
	//TODO open config file
	//Ogre::ConfigFile cfg;
	//cfg.load("");

	OgreFramework::getSingletonPtr()->m_pLog->logMessage("Creating GameScene...");
	//m_pSceneMgr->setSkyBox(true, "Examples/MorningSkyBox");
	
	m_pSceneMgr->setAmbientLight(Ogre::ColourValue(0.3, 0.3, 0.3));
	//m_pSceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
    m_pSceneMgr->createLight()->setPosition(250, 10, 250);
	m_pSceneMgr->createLight()->setPosition(250, 10, -250);
	m_pSceneMgr->createLight()->setPosition(-250, 10, 250);
	m_pSceneMgr->createLight()->setPosition(-250, 10, -250);
	Ogre::SceneNode* node = NULL;

	Ogre::Plane plane(Ogre::Vector3::UNIT_Y, 0);
	Ogre::MeshManager::getSingleton().createPlane("ground", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
		plane, 1500, 1500, 20, 20, true, 1, 5, 5, Ogre::Vector3::UNIT_Z);
	Ogre::Entity* ent = m_pSceneMgr->createEntity("GroundEntity", "ground");
	ent->setMaterialName("Examples/Rockwall");
	node = m_pSceneMgr->getRootSceneNode()->createChildSceneNode();
	node->setScale(Ogre::Vector3(100, 100, 100));
	node->attachObject(ent);

	plane = Ogre::Plane(-Ogre::Vector3::UNIT_Y, 0);
	Ogre::MeshManager::getSingleton().createPlane("up", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
		plane, 1500, 1500, 20, 20, true, 1, 5, 5, Ogre::Vector3::UNIT_Z);
	ent = m_pSceneMgr->createEntity("GroundEntity2", "up");
	ent->setMaterialName("Examples/Rockwall");
	node = m_pSceneMgr->getRootSceneNode()->createChildSceneNode();
	node->setScale(Ogre::Vector3(100, 100, 100));
	node->setPosition(0, 40, 0);
	node->attachObject(ent);

	Ogre::Plane wallRight(Ogre::Vector3::UNIT_Z,0);
	Ogre::MeshManager::getSingleton().createPlane("wallRight", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
    wallRight, 1500, 1500, 20, 20, true, 1, 5, 5, Ogre::Vector3::UNIT_Y);
	ent = m_pSceneMgr->createEntity("WallRightEntity", "wallRight");
	ent->setMaterialName("Examples/Rockwall");
	node = m_pSceneMgr->getRootSceneNode()->createChildSceneNode();
	node->setScale(Ogre::Vector3(100, 100, 100));
	node->setPosition(0, 0, -SIDE_LIMIT-SIDE_MARGE);
	node->attachObject(ent);
	
	Ogre::Plane wallLeft(-Ogre::Vector3::UNIT_Z,0);
	Ogre::MeshManager::getSingleton().createPlane("wallLeft", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
    wallLeft, 1500, 1500, 20, 20, true, 1, 5, 5, Ogre::Vector3::UNIT_Y);
	ent = m_pSceneMgr->createEntity("WallLeftEntity", "wallLeft");
	ent->setMaterialName("Examples/Rockwall");
	node = m_pSceneMgr->getRootSceneNode()->createChildSceneNode();
	node->setScale(Ogre::Vector3(100, 100, 100));
	node->setPosition(0, 0, SIDE_LIMIT+SIDE_MARGE);
	node->attachObject(ent);

	m_pLevelReader = new LevelReader(m_pLevelName, m_pSceneMgr, &m_pCheckpoints);
	m_pLevelReader->setAntList(&m_pAntList);
	m_pLevelReader->setEnd(&end);
	m_pLevelReader->setTutorial(&tutorial);
	node = m_pSceneMgr->getRootSceneNode()->createChildSceneNode("Coin");


	m_pLevelReader->readFile();

	/*ent = m_pSceneMgr->createEntity("Machine", "ABY_Machine_01.mesh");
	node = m_pSceneMgr->getRootSceneNode()->createChildSceneNode();
	node->setPosition(-100, 0, -8);
	node->setScale(0.05, 0.05, 0.05);
	node->rotate(Ogre::Vector3(1, 0, 0), Ogre::Radian(Ogre::Math::PI/2));
	node->rotate(Ogre::Vector3(0, 1, 0), Ogre::Radian(Ogre::Math::PI),Ogre::Node::TS_WORLD);
	node->attachObject(ent);

	ent = m_pSceneMgr->createEntity("CoinEntity", "TyveKrone.mesh");
	
	node->setPosition(-100, 4, 5);
	node->setScale(1.5, 1.5, 1.5);
	node->rotate(Ogre::Vector3(0, 1, 0), Ogre::Radian(Ogre::Math::PI/2),Ogre::Node::TS_WORLD);
	node->attachObject(ent);
	*/

	

	OgreFramework::getSingletonPtr()->m_pLog->logMessage("Creating Character...");
	m_pChara = new Avatar("Ninja", m_pSceneMgr);
	m_pChara->setCheckpoints(&m_pCheckpoints);
	m_pChara->setType(mBeatType);
	m_pChara->setCOM(mCom);
	m_pChara->setupAll(m_pSceneMgr);
	if(mWrite)
	{
		m_pChara->write = true;
		m_pChara->mDataWrite = m_pWriteDataName;
	}
	else
	{
		m_pChara->write = false;
	}
	OgreFramework::getSingletonPtr()->m_pLog->logMessage("Creating Camera...");
	m_pMalCamera = new MalkohaCamera("MalkohaCamera", m_pSceneMgr, m_pCamera);

	OgreFramework::getSingletonPtr()->m_pLog->logMessage("Finish Creating GameScene...");
}

//|||||||||||||||||||||||||||||||||||||||||||||||

bool GameState::keyPressed(const OIS::KeyEvent &keyEventRef)
{

    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_ESCAPE))
    {
        pushAppState(findByName("PauseState"));
        return true;
    }
	m_pChara->injectKeyDown(keyEventRef);
    return true;
}

//|||||||||||||||||||||||||||||||||||||||||||||||

bool GameState::keyReleased(const OIS::KeyEvent &keyEventRef)
{
	OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);
	m_pChara->injectKeyUp(keyEventRef);
    return true;
}

//|||||||||||||||||||||||||||||||||||||||||||||||

bool GameState::mouseMoved(const OIS::MouseEvent &evt)
{
	if(OgreFramework::getSingletonPtr()->m_pTrayMgr->injectMouseMove(evt)) return true;
	m_pChara->injectMouseMove(evt);
    return true;
}

//|||||||||||||||||||||||||||||||||||||||||||||||

bool GameState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	if(OgreFramework::getSingletonPtr()->m_pTrayMgr->injectMouseDown(evt, id)) return true;
	m_pChara->injectMouseDown(evt, id);
    return true;
}

//|||||||||||||||||||||||||||||||||||||||||||||||

bool GameState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	if(OgreFramework::getSingletonPtr()->m_pTrayMgr->injectMouseDown(evt, id)) return true;
	m_pChara->injectMouseDown(evt, id);
    return true;
}

//|||||||||||||||||||||||||||||||||||||||||||||||


//|||||||||||||||||||||||||||||||||||||||||||||||

void GameState::update(double timeSinceLastFrame)
{
	m_FrameEvent.timeSinceLastFrame = timeSinceLastFrame;
    OgreFramework::getSingletonPtr()->m_pTrayMgr->frameRenderingQueued(m_FrameEvent);
	
	if(m_bQuit == true)
    {
        popAppState();
        return;
    }
	if(m_pChara)
	{
		m_pChara->update(timeSinceLastFrame);
		if(m_pMalCamera)
		{
			
			m_pMalCamera->update (timeSinceLastFrame, 
				Ogre::Vector3(m_pChara->getCameraNode()->_getDerivedPosition().x, CAM_HEIGHT, 0), 
				Ogre::Vector3(m_pChara->getSightNode()->_getDerivedPosition().x, CHAR_HEIGHT, 0));

		}
	}
	// Update the coins
	Ogre::SceneNode* coin = m_pSceneMgr->getSceneNode("Coin");
	Ogre::Node::ChildNodeIterator it = coin->getChildIterator();
	while( it.hasMoreElements() )
	{
		it.getNext()->yaw( Ogre::Radian(timeSinceLastFrame/1000/COIN_PERIOD * 2*Ogre::Math::PI));
	}

	//update ants
	if(!m_pChara->QTEmode)
	{
		int ax = m_pChara->getWorldPosition().x;
		int az = m_pChara->getWorldPosition().z;
		std::vector<Ant*>::iterator ant;
		if(!m_pChara->QTEmode)
		{
			for(ant = m_pAntList.begin();ant != m_pAntList.end(); ant++)
			{
				if(!(*ant)->dead && ax < (*ant)->getPosition().x && ax > (*ant)->getPosition().x-3)
				{
					if(az < (*ant)->getPosition().z + 3 && az > (*ant)->getPosition().z - 3)
					{
						OgreFramework::getSingletonPtr()->m_pLog->logMessage("QTE");
						m_pChara->enterQTEMode(*ant);
					}
				}
				//TODO remove old ant
			}
		}
	}
	//Check if end of level
	if(end > m_pChara->getWorldPosition().x)
	{
		OgreFramework::getSingletonPtr()->m_pTrayMgr->moveWidgetToTray("TutoLbl",OgreBites::TL_CENTER);
		char buffer[256];
		sprintf(buffer, "Final Score : %d", m_pChara->score);
		((OgreBites::Label*) OgreFramework::getSingletonPtr()->m_pTrayMgr->getWidget("TutoLbl"))->setCaption(buffer);
		OgreFramework::getSingletonPtr()->m_pLog->logMessage(buffer);
		popAllAndPushAppState(findByName(mNextLevel));
	}
	//Check if tutorial
	std::vector<Tutorial*>::iterator t;
	for(t = tutorial.begin(); t != tutorial.end(); t++)
	{
		float x = m_pChara->getWorldPosition().x;
		if((*t)->pos > x && (*t)->pos < x+5)
		{
			if(!(*t)->dead)
			{
				(*t)->execute(&m_pChara->begin);
				(*t)->dead = true;
			}
		}
	}
}


//|||||||||||||||||||||||||||||||||||||||||||||||

void GameState::setLevelName(Ogre::String levelName)
{
	m_pLevelName = levelName;
}
void GameState::setType(BEAT beat)
{
	mBeatType = beat;
}
void GameState::setCom(Ogre::String com)
{
	mCom = com;
}
void GameState::setNextLevel(Ogre::String next)
{
	mNextLevel = next;
}
void GameState::setDataToWrite(Ogre::String name, bool write)
{
	m_pWriteDataName = name;
	mWrite = write;
}