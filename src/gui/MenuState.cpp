//|||||||||||||||||||||||||||||||||||||||||||||||

#include "MenuState.hpp"
//|||||||||||||||||||||||||||||||||||||||||||||||

using namespace Ogre;

//|||||||||||||||||||||||||||||||||||||||||||||||

/*
Configuration options:
Port serial : COM...
Tutorial: tutorial.lvl 1hzglow
Level1: level1.lvl sync
Level2: level2.lvl async

*/
MenuState::MenuState()
{
    m_bQuit         = false;
    m_FrameEvent    = Ogre::FrameEvent();
	mTrayMgr = OgreFramework::getSingletonPtr()->m_pTrayMgr;
}

//|||||||||||||||||||||||||||||||||||||||||||||||

void MenuState::enter()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Entering MenuState...");

    m_pSceneMgr = OgreFramework::getSingletonPtr()->m_pRoot->createSceneManager(ST_GENERIC, "MenuSceneMgr");
    m_pSceneMgr->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));

    //m_pSceneMgr->addRenderQueueListener(OgreFramework::getSingletonPtr()->m_pOverlaySystem);

    m_pCamera = m_pSceneMgr->createCamera("MenuCam");
    m_pCamera->setPosition(Vector3(0, 25, -50));
    m_pCamera->lookAt(Vector3(0, 0, 0));
    m_pCamera->setNearClipDistance(1);

    m_pCamera->setAspectRatio(Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualWidth()) /
        Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualHeight()));

    OgreFramework::getSingletonPtr()->m_pViewport->setCamera(m_pCamera);

	

	setupWidgets();
    createScene();
}

//|||||||||||||||||||||||||||||||||||||||||||||||
void MenuState::setupWidgets()
{
	mTrayMgr->destroyAllWidgets();
    //mTrayMgr->showFrameStats(OgreBites::TL_BOTTOMLEFT);
    mTrayMgr->showLogo(OgreBites::TL_BOTTOMRIGHT);
    mTrayMgr->showCursor();


    mTrayMgr->createButton(OgreBites::TL_CENTER, "EnterBtn", "Enter Game", 250);
	mTrayMgr->createButton(OgreBites::TL_CENTER, "Configure", "Configure", 250);
    mTrayMgr->createButton(OgreBites::TL_CENTER, "ExitBtn", "Exit Malkoha", 250);
	mTrayMgr->createLabel(OgreBites::TL_TOP, "MenuLbl", "Menu", 250);
		char buffer[256];
	sprintf(buffer, "You are number %d", ID);
	mTrayMgr->createLabel(OgreBites::TL_TOP, "Hello", Ogre::String(buffer), 250);
	// create configuration screen button tray

	mTrayMgr->createButton(OgreBites::TL_NONE, "Back", "Go Back");

	Ogre::StringVector portList;
	for(int i = 0; i<11; i++)
	{
		char buffer[6];
		sprintf(buffer,"COM%d",i);
		portList.push_back(Ogre::String(buffer));
	}
	mTrayMgr->createThickSelectMenu(OgreBites::TL_NONE, "PortLbl", "Serial Port: ",150, 10, portList);
	Ogre::StringVector glowList;
	glowList.push_back(Ogre::String("None"));
	glowList.push_back(Ogre::String("1HZ"));
	glowList.push_back(Ogre::String("Sync"));
	glowList.push_back(Ogre::String("Async"));
	mTrayMgr->createThickSelectMenu(OgreBites::TL_NONE, "TutLbl", "Tutorial: ",150,5,glowList);
	((OgreBites::SelectMenu*)mTrayMgr->getWidget("TutLbl"))->selectItem("1HZ",false);
	mTrayMgr->createThickSelectMenu(OgreBites::TL_NONE, "Lvl1Lbl", "Level 1: ",150,5,glowList);
	((OgreBites::SelectMenu*)mTrayMgr->getWidget("Lvl1Lbl"))->selectItem("Sync",false);
	mTrayMgr->createThickSelectMenu(OgreBites::TL_NONE, "Lvl2Lbl", "Level 2: ",150,5,glowList);
	((OgreBites::SelectMenu*)mTrayMgr->getWidget("Lvl2Lbl"))->selectItem("Async",false);

	mTrayMgr->showFrameStats(OgreBites::TL_BOTTOMLEFT);

	mTrayMgr->getTrayContainer(OgreBites::TL_NONE)->hide();
}
void MenuState::createScene()
{
}

//|||||||||||||||||||||||||||||||||||||||||||||||

void MenuState::exit()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Leaving MenuState...");

    m_pSceneMgr->destroyCamera(m_pCamera);
    if(m_pSceneMgr)
        OgreFramework::getSingletonPtr()->m_pRoot->destroySceneManager(m_pSceneMgr);

    mTrayMgr->clearAllTrays();
    mTrayMgr->destroyAllWidgets();
    mTrayMgr->setListener(0);
}

//|||||||||||||||||||||||||||||||||||||||||||||||

bool MenuState::keyPressed(const OIS::KeyEvent &keyEventRef)
{
    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_ESCAPE))
    {
        m_bQuit = true;
        return true;
    }

    OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);
    return true;
}

//|||||||||||||||||||||||||||||||||||||||||||||||

bool MenuState::keyReleased(const OIS::KeyEvent &keyEventRef)
{
    OgreFramework::getSingletonPtr()->keyReleased(keyEventRef);
    return true;
}

//|||||||||||||||||||||||||||||||||||||||||||||||

bool MenuState::mouseMoved(const OIS::MouseEvent &evt)
{
    if(mTrayMgr->injectMouseMove(evt)) return true;
    return true;
}

//|||||||||||||||||||||||||||||||||||||||||||||||

bool MenuState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    if(mTrayMgr->injectMouseDown(evt, id)) return true;
    return true;
}

//|||||||||||||||||||||||||||||||||||||||||||||||

bool MenuState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    if(mTrayMgr->injectMouseUp(evt, id)) return true;
    return true;
}

//|||||||||||||||||||||||||||||||||||||||||||||||

void MenuState::update(double timeSinceLastFrame)
{
    m_FrameEvent.timeSinceLastFrame = timeSinceLastFrame;
    mTrayMgr->frameRenderingQueued(m_FrameEvent);

    if(m_bQuit == true)
    {
        shutdown();
        return;
    }
}

//|||||||||||||||||||||||||||||||||||||||||||||||

void MenuState::buttonHit(OgreBites::Button *button)
{
    if(button->getName() == "ExitBtn")
	{
        m_bQuit = true;
	}
    else if(button->getName() == "EnterBtn")
	{
        changeAppState(findByName("Tutorial"));
	}
	else if (button->getName() == "Configure")   // enter configuration screen
	{
		
		mTrayMgr->removeWidgetFromTray("ExitBtn");
		mTrayMgr->removeWidgetFromTray("EnterBtn");
		mTrayMgr->removeWidgetFromTray("Configure");
		mTrayMgr->moveWidgetToTray("Back", OgreBites::TL_RIGHT);
		mTrayMgr->moveWidgetToTray("PortLbl", OgreBites::TL_CENTER);
		mTrayMgr->moveWidgetToTray("TutLbl", OgreBites::TL_CENTER);
		mTrayMgr->moveWidgetToTray("Lvl1Lbl", OgreBites::TL_CENTER);
		mTrayMgr->moveWidgetToTray("Lvl2Lbl", OgreBites::TL_CENTER);
		

	}
	else if (button->getName() == "Back")   // leave configuration screen
	{
		mTrayMgr->removeWidgetFromTray("Back");
		mTrayMgr->removeWidgetFromTray("PortLbl");
		mTrayMgr->removeWidgetFromTray("TutLbl");
		mTrayMgr->removeWidgetFromTray("Lvl1Lbl");
		mTrayMgr->removeWidgetFromTray("Lvl2Lbl");

		mTrayMgr->moveWidgetToTray("EnterBtn", OgreBites::TL_CENTER);
		mTrayMgr->moveWidgetToTray("Configure", OgreBites::TL_CENTER);
		mTrayMgr->moveWidgetToTray("ExitBtn", OgreBites::TL_CENTER);
	}
}
/*-----------------------------------------------------------------------------
| Handles menu item selection changes.
-----------------------------------------------------------------------------*/
void MenuState::itemSelected(OgreBites::SelectMenu* menu)
{
	if(menu->getName() == "PortLbl")
	{
		close();
		closeSerial();
		if(0!=openSerial(Ogre::String(menu->getSelectedItem()).c_str()))
		{
			mTrayMgr->showOkDialog("COM Error", Ogre::String("Error with ")+menu->getSelectedItem());
		}
		else
		{

			mTrayMgr->showOkDialog("COM", "Connexion made successfully");
			((GameState*)m_pAppStateManager->findByName("Tutorial"))->setCom(menu->getSelectedItem());
			((GameState*)m_pAppStateManager->findByName("Level1"))->setCom(menu->getSelectedItem());
			((GameState*)m_pAppStateManager->findByName("Level2"))->setCom(menu->getSelectedItem());
			openSerial(Ogre::String(menu->getSelectedItem()).c_str());
			if(*loop != NULL)
			{
				delete *loop;
				
			}
			*loop = new sf::Thread(&serialMainLoop);
			(*loop)->launch();
		}
		
	}
	else
	{
		GameState* game = NULL;
		if(menu->getName() == "TutLbl")
		{
			game = ((GameState*)m_pAppStateManager->findByName("Tutorial"));
		}
		else if (menu->getName() == "Lvl1Lbl")
		{
			game = ((GameState*)m_pAppStateManager->findByName("Level1"));
		}
		else if(menu->getName() == "Lvl2Lbl")
		{
			game = ((GameState*)m_pAppStateManager->findByName("Level2"));
		}

		//Modify Game
		if(game != NULL)
		{
			if(menu->getSelectedItem() == "None")
			{
				game->setType(NONE);
			}
			else if(menu->getSelectedItem() == "1HZ")
			{
				game->setType(HZ);
			}
			else if(menu->getSelectedItem() == "Sync")
			{
				game->setType(SYNC);
			}
			else if(menu->getSelectedItem() == "Async")
			{
				game->setType(ASYNC);
			}

		}
		mTrayMgr->showOkDialog("Game Config", menu->getCaption()+menu->getSelectedItem());
	}
}

void MenuState::setAppStateManager(AppStateManager* appStateManager)
{
	m_pAppStateManager = appStateManager;
}