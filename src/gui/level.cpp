
#include "level.h"

LevelReader::LevelReader(Ogre::String name, Ogre::SceneManager* sceneMgr, std::vector<int>* checkpoints) 
{
	mCheckpoints = checkpoints;
	mSceneMgr = sceneMgr;
	mName = name;
}

LevelReader::~LevelReader() {}

int LevelReader::readFile() {
	int status = 0;
	FILE *f = fopen(mName.c_str(),"r");
	if (f) {
		OgreFramework::getSingletonPtr()->m_pLog->logMessage(Ogre::String("Successfully opened : ")+mName);
		while(!readLine(f));
		fclose(f);
	}
	else
	{
		OgreFramework::getSingletonPtr()->m_pLog->logMessage(Ogre::String("Erreur while opening file : ")+mName);
		status = errno;
	}

	return status;
}

int LevelReader::readLine(FILE *f) {
	Ogre::Entity* ent = NULL;
	Ogre::SceneNode* node = NULL;
	char name[1024];
	char mesh[1024];
	char buffer[1024];
	float x,y;
	float ang1, ang2, scale;
	if (1!=fscanf(f,"%[^\n]\n",buffer)) { //eof
		return -1;
	}

	int len=-1;

	bool last=false;
	for (int i=0;i<1024&&len==-1;++i) {
		if (buffer[i]=='/') {
			if (last) 
				len=i-1;
			else
				last=true;
		}
		else
			last=false;
	}
	if (len == 0) { //empty line, skip
		return 0;
	}
	if (len!=-1) {
		buffer[len] = '\0'; //trim the comment
	}
	int result = sscanf(buffer,"%1024[^,] , %1024[^,] , %f , %f , %f , %f , %f \n",name,mesh,&x,&y,&ang1,&ang2,&scale);

	if (result==0) {//empty line, skip
		return 0;
	}
	if (result!=7) { //garbage, return error
		OgreFramework::getSingletonPtr()->m_pLog->logMessage(Ogre::String("Failed to parse line: ")+Ogre::String(buffer));
		return -2;
	}
	OgreFramework::getSingletonPtr()->m_pLog->logMessage(Ogre::String("Line succesfully read : ")+name);
	if(strstr(name,"Coin"))
	{
		ent = mSceneMgr->createEntity(name, mesh);
		node = mSceneMgr->getSceneNode("Coin")->createChildSceneNode();
		node->setPosition(x, 4, y);
		node->setScale(scale,scale,scale);
		node->rotate(Ogre::Vector3(1, 0, 0), Ogre::Radian(Ogre::Math::PI*ang1));
		node->rotate(Ogre::Vector3(0, 1, 0), Ogre::Radian(Ogre::Math::PI*ang2),Ogre::Node::TS_WORLD);
		node->attachObject(ent);
	}
	else if(strstr(name, "Checkpoint"))
	{
		mCheckpoints->push_back(x);
	}
	else if(strstr(name, "End"))
	{
		*mEnd = x;
	}
	else if(strstr(name, "Ant"))
	{
		Ant* ant = new Ant(name, mSceneMgr);
		ent = mSceneMgr->createEntity(name, mesh);
		node = mSceneMgr->getRootSceneNode()->createChildSceneNode();
		node->setPosition(x, 40, y);
		node->setScale(scale,scale,scale);
		node->rotate(Ogre::Vector3(1, 0, 0), Ogre::Radian(Ogre::Math::PI*ang1));
		node->rotate(Ogre::Vector3(0, 1, 0), Ogre::Radian(Ogre::Math::PI*ang2),Ogre::Node::TS_WORLD);
		node->attachObject(ent);
		ant->setEntity(ent);
		ant->setNode(node);
		ant->setupAnimation();
		mAntList->push_back(ant);
	}
	else if(strstr(name, "Tutorial"))
	{
		Tutorial* t = new Tutorial();
		t->text = mesh;
		t->pos = x;
		mTutorial->push_back(t);
	}
	else
	{
		ent = mSceneMgr->createEntity(name, mesh);
		node = mSceneMgr->getRootSceneNode()->createChildSceneNode();
		node->setPosition(x, 0, y);
		node->setScale(scale,scale,scale);
		node->rotate(Ogre::Vector3(1, 0, 0), Ogre::Radian(Ogre::Math::PI*ang1));
		node->rotate(Ogre::Vector3(0, 1, 0), Ogre::Radian(Ogre::Math::PI*ang2),Ogre::Node::TS_WORLD);
		node->attachObject(ent);

	}
	return 0;
}
void LevelReader::setAntList(std::vector<Ant*>* antlist)
{
	mAntList = antlist;
}
void LevelReader::setEnd(int* end)
{
	mEnd = end;
}
void LevelReader::setTutorial(std::vector<Tutorial*>* tutorial)
{
	mTutorial = tutorial;
}