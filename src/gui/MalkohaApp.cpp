//|||||||||||||||||||||||||||||||||||||||||||||||

#include "MalkohaApp.hpp"

//|||||||||||||||||||||||||||||||||||||||||||||||

MalkohaApp::MalkohaApp():mLoop(NULL), mAsyncLoop(NULL)
{
	m_pAppStateManager = 0;
}

//|||||||||||||||||||||||||||||||||||||||||||||||

MalkohaApp::~MalkohaApp()
{
	delete m_pAppStateManager;
    delete OgreFramework::getSingletonPtr();
	close();
	closeAsync();

	if(mLoop != NULL)
		delete mLoop;
	if(mAsyncLoop != NULL)
		delete mAsyncLoop;
}

//|||||||||||||||||||||||||||||||||||||||||||||||

void MalkohaApp::go()
{
	std::srand(std::time(0));
	int ID = std::rand();
	new OgreFramework();
	if(!OgreFramework::getSingletonPtr()->initOgre("MalkohaApp", 0, 0))
		return;

	OgreFramework::getSingletonPtr()->m_pLog->logMessage("Malkoha initialized!");

	m_pAppStateManager = new AppStateManager();

	MenuState::create(m_pAppStateManager, "MenuState");
	((MenuState*)m_pAppStateManager->findByName("MenuState"))->setAppStateManager(m_pAppStateManager);

	GameState::create(m_pAppStateManager, "Tutorial");
	((GameState*)m_pAppStateManager->findByName("Tutorial"))->setLevelName("media/level/tutorial.lvl");
	((GameState*)m_pAppStateManager->findByName("Tutorial"))->setType(HZ);
	((GameState*)m_pAppStateManager->findByName("Tutorial"))->setNextLevel("Level1");

	GameState::create(m_pAppStateManager, "Level1");
	((GameState*)m_pAppStateManager->findByName("Level1"))->setNextLevel("Level2");
	
	GameState::create(m_pAppStateManager, "Level2");
	((GameState*)m_pAppStateManager->findByName("Level2"))->setNextLevel("MenuState");
	if(ID%2)
	{
		((GameState*)m_pAppStateManager->findByName("Level1"))->setLevelName("media/level/level1.lvl");
		((GameState*)m_pAppStateManager->findByName("Level2"))->setLevelName("media/level/level1m.lvl");
	}
	else
	{
		((GameState*)m_pAppStateManager->findByName("Level1"))->setLevelName("media/level/level2.lvl");
		((GameState*)m_pAppStateManager->findByName("Level2"))->setLevelName("media/level/level2m.lvl");
	}
	((GameState*)m_pAppStateManager->findByName("Level1"))->setType(SYNC);
	((GameState*)m_pAppStateManager->findByName("Level2"))->setType(ASYNC);

	PauseState::create(m_pAppStateManager, "PauseState");
	char buffer[1024];
	sprintf(buffer, "data/Log-%d-", ID);
	((GameState*)m_pAppStateManager->findByName("Tutorial"))->setDataToWrite(Ogre::String(buffer), false);
	((GameState*)m_pAppStateManager->findByName("Level1"))->setDataToWrite(Ogre::String(buffer)+Ogre::String("Level1.log"), true);
	((GameState*)m_pAppStateManager->findByName("Level2"))->setDataToWrite(Ogre::String(buffer)+Ogre::String("Level2.log"), true);

	((MenuState*)m_pAppStateManager->findByName("MenuState"))->loop = &mLoop;
	((MenuState*)m_pAppStateManager->findByName("MenuState"))->ID = ID;
	if(0==openAsyncDataFile("media/async/async.txt", true))
	{
		mAsyncLoop = new sf::Thread(&aSyncMainLoop);
		mAsyncLoop->launch();
	}
	m_pAppStateManager->start(m_pAppStateManager->findByName("MenuState"));
}

//|||||||||||||||||||||||||||||||||||||||||||||||