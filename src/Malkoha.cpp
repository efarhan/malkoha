#include <MalkohaApp.hpp>
#include "iostream"
#include "hid.h"
#include "serial.h"
#include <SFML/System.hpp>
#include <AdvancedOgreFramework.hpp>

#if OGRE_PLATFORM == PLATFORM_WIN32 || OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN

#include "windows.h"

//|||||||||||||||||||||||||||||||||||||||||||||||
 
INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
int main(int argc, char **argv)
#endif
{
	// Create application object
	MalkohaApp app;
	//Open the hid and begin the loop for data input
	sf::Thread inputThread(&openHID);
	inputThread.launch();
	
	
	try {
		app.go();
		
	} catch( Ogre::Exception& e ) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox( NULL, e.getFullDescription().c_str(), "An exception has occured!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occured: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }
	/* Terminate the input loop
	*/
	closeApp();

	inputThread.wait();
    return 0;
}
//|||||||||||||||||||||||||||||||||||||||||||||||