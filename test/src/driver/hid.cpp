#include <hid.h>

#define THETA_MIN 1200
#define THETA_MAX 1500
#define THETA_SCALE 300
#define LR_LEFT 1500
#define LR_ORIGIN 1800
#define LR_RIGHT 2100
#define LR_SCALE 300
#define FB_FRONT 1100
#define FB_ORIGIN 1400
#define FB_BACK 1700
#define FB_SCALE 300


static sf::Mutex closeLock;
static sf::Mutex dataLock;
static bool closeThread;
static hid_device* handle;
static int* data = NULL;
static Data* externData = NULL;

bool isClose()
{
	bool close = false;
	closeLock.lock();
	close = closeThread;
	closeLock.unlock();
	return close;

}
void printRawData(int* data) {
	int i=0;
	printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	printf("array size=%d\n",data[0]);
	for (i=1;i<data[0]+1;++i) {
		printf("data[%d]=%d\n",i-1,data[i]);
	}

}

Data* getData()
{
	
	if (externData) {
		free(externData);
		externData = NULL;
	}
	dataLock.lock();
	if (data) {
		externData = (Data*)malloc(sizeof(Data));
		if (externData) {
			int tmp=0;
			// data[2] angle from vertical axis
			tmp = data[2]-THETA_MIN;
			if (tmp > THETA_SCALE) {
				tmp = THETA_SCALE;
			}
			else if  (tmp < 0) {
				tmp = 0;
			}
			externData->angle = ((float)tmp)/300;


			tmp = data[3]-LR_ORIGIN;
			if (tmp > LR_SCALE) {
				tmp = LR_SCALE;
			}
			else if  (tmp < -LR_SCALE) {
				tmp = -LR_SCALE;
			}
			externData->x = ((float)tmp)/300;


			tmp = data[4]-FB_ORIGIN;
			if (tmp > FB_SCALE) {
				tmp = FB_SCALE;
			}
			else if  (tmp < -FB_SCALE) {
				tmp = -FB_SCALE;
			}
			externData->y = -((float)tmp)/300;
			

			externData->button = data[6]>2000;


		}
	}
	dataLock.unlock();
	return externData;
}
void setData(int* newData)
{
	dataLock.lock();
	free(data);
	data = newData;
	dataLock.unlock();
}
int openHID()
{
	
	#define MAX_STR 255
	wchar_t wstr[MAX_STR];
	int res;
	struct hid_device_info *devs, *cur_dev;
	
	if (hid_init())
		return -1;

	devs = hid_enumerate(0x0, 0x0);
	cur_dev = devs;

	while (cur_dev) {
		printf( "Device Found\n  type: %04hx %04hx\n  path: %s\n  serial_number: %ls", cur_dev->vendor_id, cur_dev->product_id, cur_dev->path, cur_dev->serial_number);
		printf( "\n");
		printf( "  Manufacturer: %ls\n", cur_dev->manufacturer_string);
		printf( "  Product:      %ls\n", cur_dev->product_string);
		printf( "  Release:      %hx\n", cur_dev->release_number);
		printf( "  Interface:    %d\n",  cur_dev->interface_number);
		printf( "\n");
		cur_dev = cur_dev->next;
	}
	hid_free_enumeration(devs);
	
	
	// Open the device using the VID, PID,
	// and optionally the Serial number.
	////handle = hid_open(0x4d8, 0x3f, L"12345");
	handle = hid_open(0x1043, 0x0015, NULL);
	if (!handle) {
		printf( "unable to open device\n");
 		return 1;
	}

	// Read the Manufacturer String
	wstr[0] = 0x0000;
	res = hid_get_manufacturer_string(handle, wstr, MAX_STR);
	if (res < 0)
		printf("Unable to read manufacturer string\n");
	printf("Manufacturer String: %ls\n", wstr);

	// Read the Product String
	wstr[0] = 0x0000;
	res = hid_get_product_string(handle, wstr, MAX_STR);
	if (res < 0)
		printf("Unable to read product string\n");
	printf("Product String: %ls\n", wstr);

	// Read the Serial Number String
	wstr[0] = 0x0000;
	res = hid_get_serial_number_string(handle, wstr, MAX_STR);
	if (res < 0)
		printf("Unable to read serial number string\n");
	printf("Serial Number String: (%d) %ls", wstr[0], wstr);
	printf("\n");

	// Read Indexed String 1
	wstr[0] = 0x0000;
	res = hid_get_indexed_string(handle, 1, wstr, MAX_STR);
	if (res < 0)
		printf("Unable to read indexed string 1\n");
	printf("Indexed String 1: %ls\n", wstr);

	loopHID();

	return 0;
}
void writeData(int res, unsigned char* buf)
{
	int* newData = NULL;
	newData = (int*)calloc(res/2+1, sizeof(int));
	if (newData) {

		//First number is the size and then we put the data
		newData[0]=res/2;
		for(int i = 0; i<res/2; i++)
		{
			newData[i+1]=buf[2*i]+256*buf[2*i+1];
		}
		setData(newData);
	}

}
int loopHID()
{
	int res;
	unsigned char buf[256];
		// Set the hid_read() function to be non-blocking.
	hid_set_nonblocking(handle, 1);
	// Read requested state. hid_read() has been set to be
	// non-blocking by the call to hid_set_nonblocking() above.
	// This loop demonstrates the non-blocking nature of hid_read().
	res = 0;
	while (!isClose()) {
		res = hid_read(handle, buf, sizeof(buf));
		if (res > 0)
			writeData(res, buf);
		else if (res < 0)
			printf("Unable to read()\n");
		else
			printf("waiting...\n");
		#ifdef WIN32
		Sleep(100/6);
		#else
		usleep(500*1000);
		#endif

	}
	if (data) {
		free(data);
		data = NULL;
	}
	if (externData) {
		free(externData);
		externData = NULL;
	}

	hid_close(handle);

	/* Free static HIDAPI objects. */
	hid_exit();
	return 0;
}

void closeApp()
{
	closeLock.lock();
	closeThread = true;
	closeLock.unlock();
}
