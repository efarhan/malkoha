
#include "level.h"

LevelReader::LevelReader(STR_ name) 
{
	mName = name;
}

LevelReader::~LevelReader() {}

int LevelReader::readFile() {
	int status = 0;
	FILE *f = fopen(TO_CHAR(mName),"r");
	if (f) {
		while(readLine(f)!=-1);
		fclose(f);
	}
	else
	{
		printf("failed to open file!\n");
#ifndef NO_OGRE
		OgreFramework::getSingletonPtr()->m_pLog->logMessage(Ogre::String("Erreur while opening file : ")+mName);
#endif
		status = errno;
	}

	return status;
}

int LevelReader::readLine(FILE *f) {
	char name[1024];
	char buffer[1024];
	int x,y;
	float ang1, ang2;
	if (1!=fscanf(f,"%[^\n]\n",buffer)) { //eof
		return -1;
	}

	int len=-1;

	bool last=false;
	for (int i=0;i<1024&&len==-1;++i) {
		if (buffer[i]=='/') {
			if (last) 
				len=i-1;
			else
				last=true;
		}
		else
			last=false;
	}
	if (len == 0) { //empty line, skip
		return 0;
	}
	if (len!=-1) {
		buffer[len] = '\0'; //trim the comment
	}
	int result = sscanf(buffer,"%1024[^,] , %d , %d , %f , %f\n",name,&x,&y,&ang1,&ang2);

	if (result==0) {//empty line, skip
		return 0;
	}
	if (result!=5) { //garbage, return error
		printf("Failed to parse line: %s\n",buffer);
		return -2;
	}

	printf("%s \n %d \n %d \n %f \n %f\n\n",name,x,y,ang1,ang2);

	//the fuck you want with name, x and y

	return 0;
}
