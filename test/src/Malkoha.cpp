
#include "iostream"
#include "hid.h"
#include "serial.h"
#include <SFML/System.hpp>
#include <windows.h>
#include "level.h";


int convert(int toBinary,char **args) {
	return convertFile(args[0],args[1],!toBinary);
}
int record(int toBinary,char **args) {
	if (openSerial(args[1])) {
		//printf("Failed to open COM port \"%s\"!\n",args[1]);
		return -1;
	}
	if (openFile(args[0],false,!toBinary)) {
		//printf("Failed to write to file \"%s\"!\n",args[0]);
		return -1;
	}
	sf::Thread thread(&serialMainLoop);
	thread.launch();

	printf("Press Enter when you want to stop recording!\n");
	fflush(stdout);
	char c;
	while ((c = getchar()) != '\n' && c != EOF); //wait for return key

	printf("Recording has stopped!\n");

	closeSerial();
	close();

	return 0;
}


int main(int argc, char **argv)
{
	if (argc>1) {
		int (*function)(int,char**);
		int reqArgs=0;
		int toBinary=0;
		if (!strcmp(argv[1],"record")) { //record output comName
			function=&record;
			reqArgs=2;
			toBinary=0;
		}
		else if (!strcmp(argv[1],"encode")) { //encode input output
			function=&convert;
			reqArgs=2;
			toBinary=1;
		}
		else if (!strcmp(argv[1],"decode")) {//decode input output
			function=&convert;
			reqArgs=2;
			toBinary=0;
		}
		else {
			printf("Unknown instruction \"%s\"!\n",argv[1]);
			return 0;
		}
		if (argc>reqArgs+1) {
			return function(toBinary,argv+2);
		}
		else {
			printf("Too few arguments for instruction \"%s\"!\n",argv[1]);
		}
		return 0;
	}



	printf("starting...\n");


	//LevelReader level("Debug\\testfichier.txt");
	//level.readFile();


	//int lol = sscanf(buffer2);



	//test();
	//Open the hid and begin the loop for data input
	//sf::Thread hidThread(&openHID);


	//sf::Thread arduinoThread(&serialMainLoop);

	//arduinoThread.launch();
	


	//openSerial("COM5");
	/*while(true) {
		Sleep(1000);
		closeSerial();
		Sleep(1000);
		openSerial("COM5");
		
	}*/
	if (openAsyncDataFile("data\\async2.txt",true)) {
		printf("Failed to open file!\n");
	}
	sf::Thread asyncThread(&aSyncMainLoop);

	asyncThread.launch();

	while(true) {
		Sleep(10);
		printData();
	}

	Sleep(100000);
	//closeSerial();
	//close();

	/*if (error)
		return error;
	else while(true) {
		printData();
		Sleep(25);
	}*/

	
	/*printf("starting main loop\n");
	while(true) {
		printf("reading data...\n");
		Data* data = getData();
		if (!data) {
			printf("NULL pointer!\n");
		}
		else {
			printf("Position: (%f,%f)\nInclination: %f\nButton: %s\n",
				data->x,data->y,data->angle,data->button?"Yes":"No");

		}
		printf("\n");
		Sleep(500);
		

	}*/

	/* Terminate the input loop
	*/
	//arduinoThread.wait();
    return 0;
}