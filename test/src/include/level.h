
#ifndef LEVEL_H__
#define LEVEL_H__
#include <stdio.h>
#include <errno.h>

#define NO_OGRE

#ifdef NO_OGRE
#define STR_ char*
#define TO_CHAR(x) x
#else
#include <Ogre.h>
#include <AdvancedOgreFramework.hpp>
#define STR_ Ogre::String
#define TO_CHAR(x) x.c_str()
#endif


class LevelReader {

public:
	LevelReader(STR_ name);
	~LevelReader();
	
	int readFile();

private:
	STR_ mName;

	int readLine(FILE *f);





};













#endif //LEVEL_H__
